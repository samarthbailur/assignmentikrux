import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './component/Login/Login'
import DashBoard from './component/Login/DashBoard'
import Logout from './component/Login/Logout'

class App extends Component {
  render() {
    return (
      <div>
      <Router>
      <div>
      

        <Route exact path="/" component={Login} />
        <Route exact path="/dashboard" component={DashBoard} />
        <Route exact path="/logout" component={Logout} />
        
      </div>
    </Router>
        
      </div>
    );
  }
}

export default App;
