import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      redirect: false
    };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this._submitLog = this._submitLog.bind(this);
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value });

  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }
  _submitLog() {
    //alert(this.state.email + " " + this.state.password)
    if (this.state.email == 'admin') {
      if (this.state.password == '123') {
        this.setState({ redirect: true })
      }
      else {
        alert("Invalid Password")
      }
    }
    else {
      alert("Invalid User Name And Password")
    }
  }
  render() {
    const { redirect } = this.state;

     if (redirect) {
       return <Redirect to='/dashboard'/>;
     }
    return (
      <div style={{
        width: "30%", margin: 'auto', marginTop: 140,

        padding: '10px'
      }}>
        <Form >
          <Form.Group controlId="formBasicEmail">
            <Form.Label>User Name</Form.Label>
            <Form.Control type="text" placeholder="User Name" onChange={this.handleEmailChange} />
            <Form.Text className="text-muted">

            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" onChange={this.handlePasswordChange} />
          </Form.Group>

          <Button variant="primary" type="submit" onClick={this._submitLog}>
            Login
  </Button>
        </Form>
      </div>
    );
  }
}

export default Login;
