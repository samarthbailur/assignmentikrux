import React, { Component } from 'react';
import { Form, Button, Card, Badge } from 'react-bootstrap';
import { Redirect } from 'react-router'

class Logout extends Component {
    constructor(props) {
        super(props);
        this.state = 
        {
            redirect : false
        }

    }
    _login = () => {
        
        this.setState({ redirect: true })
    }

    render() {
        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to='/' />;
        }
        return (
            <div style={{alignItems: 'center',width : "25%",margin: 'auto', marginTop: 140, padding: '10px' }}>
                <h1>
                    <Badge variant="secondary">Logout Successful</Badge>
                </h1>
                <Button style={{ alignItems: 'center', width : 150, marginLeft: 60 }} onClick={this._login} variant="info">Login</Button>

            </div>
        );
    }
}

export default Logout;
