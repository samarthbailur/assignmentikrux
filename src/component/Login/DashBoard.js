import React, { Component } from 'react';
import EmpData from './EmployeeDetails.json';
import { Table, Button, Navbar, NavDropdown, FormControl, Nav, Form, Overlay, Tooltip } from 'react-bootstrap';
import { Redirect } from 'react-router'
class DashBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            redirect: false,
            Emp_data_id: '',
            show: false
        }
        this.attachRef = target => this.setState({ target });
    }
    SearchList = (event) => {

        let updatedList = EmpData;
        updatedList = updatedList.filter(function (item) {
            //  alert(JSON.stringify(updatedList));
            return item.Emp_name.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        // alert(JSON.stringify(updatedList));
        this.setState({ items: updatedList });
    }
    componentWillMount() {
        this.setState({ items: EmpData })
    }
    _logOut = () => {
        this.setState({ redirect: true })
    }
    _Tooltip(e) {
        this.setState({ show: true })
        setTimeout(() => { this.setState({ show: false }); }, 10000);
        let data = EmpData.find(item => item.Emp_id == e);
        this.setState({ Emp_data_id: data })
    }

    render() {
        const { redirect } = this.state;
        const { show, target } = this.state;
        if (redirect) {
            return <Redirect to='/logout' />;
        }
        return (
            <div>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand style={{ fontSize: 30, fontWeight: 'bold' }}>DashBoard</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">

                        <Form inline style={{ marginLeft: 150 }}>
                            <Navbar.Brand style={{ fontSize: 15, fontWeight: 'bold' }}>Search :</Navbar.Brand>
                            <FormControl type="text" placeholder="Search Employee" className="mr-sm-2" onChange={this.SearchList} />

                        </Form>
                        <Button style={{ marginLeft: '55%' }} onClick={this._logOut} variant="secondary">Logout</Button>
                    </Navbar.Collapse>
                </Navbar>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Employee Name</th>
                            <th>Designation</th>
                            <th>Salary</th>
                        </tr>
                    </thead>
                    {
                        this.state.items.map((item, index) => {
                            return (
                                <React.Fragment>

                                    <tbody>
                                        <tr ref={this.attachRef} onClick={() => this._Tooltip(item.Emp_id)}>
                                            <td>{index + 1}</td>
                                            <td>{item.Emp_name}</td>
                                            <td>{item.Designation}</td>
                                            <td>{item.Salary}</td>

                                        </tr>
                                    </tbody>
                                    <Overlay target={target} show={show} placement="right">
                                        {({ placement, ...props }) => (
                                            <div
                                                {...props}
                                                style={{
                                                    backgroundColor: 'rgba(186,186,210,0.85)',
                                                    padding: '2px 10px',
                                                   // color: 'white',
                                                    borderRadius: 3,
                                                    ...props.style,
                                                }}
                                            >
                                                Contact : {this.state.Emp_data_id.Contact}<br></br>
                                                Email : {this.state.Emp_data_id.Email}
                                            </div>
                                        )}
                                    </Overlay>
                                </React.Fragment>
                            )
                        })

                    }


                </Table>
                {/* <Button
                    variant="danger"
                    ref={this.attachRef}
                    onClick={() => this.setState({ show: !show })}
                >
                    Click me to see
        </Button>
                <Overlay target={target} show={show} placement="right">
                    {({ placement, scheduleUpdate, arrowProps, ...props }) => (
                        <div
                            {...props}
                            style={{
                                backgroundColor: 'rgba(255, 100, 100, 0.85)',
                                padding: '2px 10px',
                                color: 'white',
                                borderRadius: 3,
                                ...props.style,
                            }}
                        >
                            Simple tooltip
            </div>
                    )}
                </Overlay>
            */}
            </div>
        );
    }
}

export default DashBoard;
